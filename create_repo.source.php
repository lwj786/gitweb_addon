<?php
$password_hash = "";

echo "<meta name = \"robots\" content = \"noindex, nofollow\"/>";
echo "<meta charset = \"utf-8\">";

echo "<title>create a git repository</title>";

echo "<body style = \"width: 40em; margin: 0 auto; font-family: sans-serif\">";

echo "<h1 style = \"text-align: center; background-color:#d9d8d1\">";
echo "git init --bare _|__.git";
echo "</h1>";

if (hash("sha256", $_POST['password']) == $password_hash) {
    $cmd = "./create_repo " . $_POST['project_name'];

    if ($_POST['git_daemon_export'] == "ok") {
        $cmd = $cmd . " --git-daemon-export-ok";
    }

    if (! $_POST['description']) {
        $_POST['description'] = $_POST['project_name'];
    }
    $_POST['description'] = '"' . $_POST['description'] . "\n" . '"';
    $cmd = $cmd . " --description " . $_POST['description'];

    unset($output);
    exec($cmd, $output, $return_var);
        
    if ($return_var) {
        echo "<p style = \"text-align: center\">";
        echo "<big><b>error:</b> exec_failed</big>\n";
        echo "</p>";
    }

    foreach ($output as &$output_per_line) {
        echo "<p style = \"text-align: center\">";
        echo "<big><i>";
        echo $output_per_line;
        echo "</big></i>";
        echo "</p>";
    }
} else {
    echo "<p style = \"text-align: center\">";
    echo "<big><b>error:</b> auth_failed</big>";
    echo "</p>";
}

echo "<hr>";

echo "<form action = \"https://gitweb.url/create_repo.html\" style = \"background-color:#f6f6f0\">";
echo "<fieldset>";
echo "<legend><big><b>create repository</b></big></legend>";
echo "<p style = \"text-align: center\">";
echo "<button type = \"submit\" style = \"background-color:#d9d8d1\">goto</button>";
echo "</p>";
echo "</fieldset>";
echo "</form>";

echo "<hr>";

echo "<form action = \"https://gitweb.url\" style = \"background-color:#f6f6f0\">";
echo "<fieldset>";
echo "<legend><big><b>gitweb interface</b></big></legend>";
echo "<p style = \"text-align: center\">";
echo "<button type = \"submit\" style = \"background-color:#d9d8d1\">goto</button>";
echo "</p>";
echo "</fieldset>";
echo "</form>";

echo "</body>";
?>
