#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define true 1
#define false 0

char GIT[] = "",
    CMD_ARG[] = " init --bare ",
    REPO_ROOT[] = "",
    REPO_SUFIX[] = ".git";

int REPO_OWNER_UID = 0;

int write_file(char *path_to_file, char *content)
{
    FILE *fp = NULL;

    if (fp = fopen(path_to_file, "w")) {
        if (content) fprintf(fp, "%s", content);
        fclose(fp);
    } else {
        return false;
    }

    return true;
}

char *get_path(char *cmd, char *filename)
{
    int len;
    char *path_to_file = NULL;

    len = strlen(cmd) - (strlen(GIT) + strlen(CMD_ARG))
        + 1 + strlen(filename);

    path_to_file = malloc(sizeof(char) * (len + 1));
    *path_to_file = 0;

    len = strlen(GIT) + strlen(CMD_ARG);

    strcat(path_to_file, cmd + len),
    strcat(path_to_file, "/"), strcat(path_to_file, filename);

    return path_to_file;
}

int main(int argc, char *argv[])
{
    int git_daemon_export = false, len;
    char *options[2] = {"git-daemon-export-ok", "description"},
        *description = NULL, *cmd = NULL, *path_to_file = NULL;

    for (++argv; --argc > 0; ++argv) {
        if ((*argv)[0] == '-' && (*argv)[1] == '-') {
            if (!strcmp(*argv + 2, options[0])) {
                git_daemon_export = true;
            } else if (!strcmp(*argv + 2, options[1])) {
                if (argc > 1)
                    if ((*(argv + 1))[0] != '-' && (*(argv + 1))[1] != '-')
                        --argc, description = *++argv;
            }
        } else {
            len = strlen(GIT) + strlen(CMD_ARG)
                + strlen(REPO_ROOT) + strlen(*argv) + strlen(REPO_SUFIX);

            cmd = malloc(sizeof(char) * (len + 1));
            *cmd = 0;

            strcat(cmd, GIT), strcat(cmd, CMD_ARG),
            strcat(cmd, REPO_ROOT), strcat(cmd, *argv), strcat(cmd, REPO_SUFIX);
        }
    }

    setgid(REPO_OWNER_UID), setuid(REPO_OWNER_UID);

    if (cmd) {
        system(cmd);

        if (git_daemon_export)
            if (path_to_file = get_path(cmd, options[0]))
                write_file(path_to_file, NULL);
        free(path_to_file);

        if (description)
            if (path_to_file = get_path(cmd, options[1]))
                write_file(path_to_file, description);
        free(path_to_file);

        free(cmd);
    }

    return 0;
}
