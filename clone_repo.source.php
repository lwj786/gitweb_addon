<?php
$support_git_protocols = array();
$repo_owner = "";
$git_server_url = "";
$repo_root = "";
$has_set_base_path =;

echo "<meta name = \"robots\" content = \"noindex, nofollow\"/>";
echo "<meta charset = \"utf-8\">";

echo "<title>clone git repository</title>";

echo "<body style = \"width: 75%; margin: 0 auto; font-family: sans-serif\">";

echo "<h1 style = \"text-align: center; background-color:#d9d8d1\">";
echo "git clone :/@.git";
echo "</h1>";

if ($repo_array = scandir($repo_root)) {
    echo "<table style = \"margin: 1em\">";

    echo "<tr>";
    echo "<th>Project</th><th colspan = \"2\">Repository Url</th>";
    echo "</tr>";

    $repo_array = array_diff($repo_array, array(".", ".."));
    $count = 0;
    foreach ($repo_array as &$repo) {
        if (++$count % 2) {
            echo "<tr style = \"background-color: #f6f6f0\">";
        } else {
            echo "<tr>";
        }

        echo "<td>" . $repo . "</td>";

        echo "<td>";
        if (in_array("git", $support_git_protocols)) {
            if (file_exists($repo_root . $repo . "/git-daemon-export-ok")) {
                if ($has_set_base_path) {
                    $repo_path = '/' . $repo;
                } else {
                    $repo_path = $repo_root . $repo;
                }
                $git_url = "git://" . $git_server_url . $repo_path;

                echo "<input type = \"radio\" name = \"git_url\" value = \"$git_url\"/>";
                echo "<i>$git_url</i>";
                echo "<br>";
            }
        }

        if (in_array("http", $support_git_protocols)) {
            if ($has_set_base_path) {
                $repo_path = '/' . $repo;
            } else {
                $repo_path = $repo_root . $repo;
            }
            $git_url = "https://" . $git_server_url . $repo_path;

            echo "<input type = \"radio\" name = \"git_url\" value = \"$git_url\"/>";
            echo "<i>$git_url</i>";
            echo "<br>";
        }

        if (in_array("ssh", $support_git_protocols)) {
            $repo_path = $repo_root . $repo;
            $git_url = $repo_owner . '@' . $git_server_url . ':' . $repo_path;

            echo "<input type = \"radio\" name = \"git_url\" value = \"$git_url\"/>";
            echo "<i>$git_url</i>";
            echo "<br>";
        }
        echo "</td>";

        if ($count == 1) {
            $repo_num = count($repo_array);
            echo "<th rowspan = \"$repo_num\">";
            echo "<textarea id = \"display\" rows = \"5\" cols = \"30\" placeholder = \"click button\" readonly></textarea>";
            echo "<br>";
            echo "<button onclick = \"copy_url()\">Copy</button>";
            echo "</th>";

            echo "<script>";
            echo "function copy_url() {";
            echo "var git_url = document.getElementsByName(\"git_url\");";
            echo "var display = document.getElementById(\"display\");";
            echo "for (var i = 0; i < git_url.length; i++) {";
            echo "if (git_url[i].checked) {";
            echo "display.value = git_url[i].value;";
            echo "display.select();";
            echo "document.execCommand(\"copy\");";
            echo "break;";
            echo "}";
            echo "}";
            echo "}";
            echo "</script>";
        }

        echo "</tr>";
    }

    echo "</table>";
}

echo "<hr>";

echo "<form action = \"https://gitweb.url\" style = \"background-color:#f6f6f0\">";
echo "<fieldset>";
echo "<legend><big><b>gitweb interface</b></big></legend>";
echo "<p style = \"text-align: center\">";
echo "<button type = \"submit\" style = \"background-color:#d9d8d1\">goto</button>";
echo "</p>";
echo "</fieldset>";
echo "</form>";

echo "</body>";
?>
