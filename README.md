# gitweb_addon
增加 gitweb 没有的功能

安装所有:
```
cp Makefile.source Makefile
editor Makefile    # 修改变量
make all
sudo make install
```

* create_repo
通过网页添加仓库（包含简单的认证，默认密码即为 password）
1. 安装：
```
editor Makefile    # 修改变量
make all-create_repo
sudo make install-create_repo
```

2. 使用
访问 gitweb.url/create_repo.html

注: commit 86b2504 message 应为 fix bug: repository uid/gid mismatch

* clone_repo
列出仓库所支持的各协议对应的 url
1. 安装：
```
editor Makefile    # 修改变量
make all-clone_repo
sudo make install-clone_repo
```

2. 使用
访问 gitweb.url/clone_repo.php
